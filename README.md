# cmplserver-single-docker
Docker image running [CMPLserver][home] from coliop in single server mode.

## Building image

```bash
docker build solvice/cmplserver-single
```

## Running container

```bash
docker run -p 8008:8008 solvice/cmplserver-single
```

CMPLserver is exposed on tcp port 8008.
 
## License

Copyright 2016 Francois Roland

cmplserver-single-docker is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

cmplserver-single-docker is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with cmplserver-single-docker.  If not, see <http://www.gnu.org/licenses/>.


[home]: http://www.coliop.org/index.html
