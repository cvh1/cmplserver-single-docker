#/bin/bash
#
# Copyright 2016 Francois Roland
#
# This file is part of cmplserver-single-docker.
#
# cmplserver-single-docker is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# cmplserver-single-docker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cmplserver-single-docker.  If not, see <http://www.gnu.org/licenses/>.

set -e

export PATH=/opt/Cmpl:/opt/Cmpl/bin:/opt/Cmpl/pyCmpl/scripts/Unix:$PATH
export CMPLBINARY=/opt/Cmpl/cmpl
export CmplProgPath=/opt/Cmpl/
export LD_LIBRARY_PATH=/opt/Cmpl/bin

exec cmplServer -start 8008 -showLog
