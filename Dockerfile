# Copyright 2016 Francois Roland
#
# This file is part of cmplserver-single-docker.
#
# cmplserver-single-docker is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# cmplserver-single-docker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with cmplserver-single-docker.  If not, see <http://www.gnu.org/licenses/>.

FROM ubuntu:16.04

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y \
    curl \
    libglib2.0-0 \
    python
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN groupadd -r cmpl && useradd -r -m -g cmpl cmpl
RUN curl http://www.coliop.org/_download/Cmpl-1-11-0-linux64.tar.gz | tar xvz -C /opt
RUN chown -R cmpl:cmpl /opt/Cmpl
COPY ./start-server.sh /home/cmpl/start-server.sh
RUN chown cmpl:cmpl /home/cmpl/start-server.sh && chmod 0755 /home/cmpl/start-server.sh

EXPOSE 8008
USER cmpl
WORKDIR /home/cmpl
CMD ["/home/cmpl/start-server.sh"]
ENTRYPOINT ["/bin/bash"]
